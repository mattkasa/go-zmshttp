FROM golang:1.17

ARG ZMSHTTP_VERSION=main

WORKDIR /zmshttp

COPY main.go go.* /zmshttp/

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o zmshttp .
