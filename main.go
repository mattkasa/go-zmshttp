package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
)

var listen, path string

func zms(resp http.ResponseWriter, req *http.Request) {
	command := exec.Command(path)

	addEnv := []string{}
	addEnv = append(addEnv, fmt.Sprintf("HTTP_REFERER=\"%s\"", req.Referer()))
	addEnv = append(addEnv, fmt.Sprintf("QUERY_STRING=\"%s\"", req.URL.RawQuery))
	command.Env = append(os.Environ(), addEnv...)

	stdout, _ := command.StdoutPipe()

	err := command.Start()
	if err != nil {
		log.Printf("command.Start() failed with '%s'\n", err)
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	var errStdout error

	go func() {
		_, errStdout = io.Copy(resp, stdout)
	}()

	err = command.Wait()
	if err != nil {
		log.Printf("command.Run() failed with '%s'\n", err)
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	if errStdout != nil {
		log.Printf("failed to capture stdout\n")
		return
	}
}

func main() {
	flag.StringVar(&listen, "listen", "127.0.0.1:8001", "listen address")
	flag.StringVar(&path, "path", "/usr/lib/zoneminder/cgi-bin/nph-zms", "path to zms binary")
	flag.Parse()

	http.HandleFunc("/", zms)
	http.ListenAndServe(listen, nil)
}
